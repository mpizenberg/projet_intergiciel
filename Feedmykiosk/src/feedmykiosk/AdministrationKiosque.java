package feedmykiosk;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.sql.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

@Stateless
public class AdministrationKiosque {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public Collection<Client> getListeClient() {
		return (Collection<Client>)em.createQuery("select c from Client c").getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<Journal> getListeJournaux() {
		return (Collection<Journal>)em.createQuery("select j from Journal j").getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<Theme> getListeThemes() {
		return (Collection<Theme>)em.createQuery("select t from Theme t").getResultList();
	}

	/**
	 * Renvoie la liste des dons (i.e la liste des journaux en la possession du client)
	 * @param c
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Journal> getClientDons(Client c){
		return (Collection<Journal>) em.createQuery("select j from Journal j join j.possesseurs p where p.username=:nom").setParameter("nom", c.getUsername()).getResultList();
	}

	public Journal getJournal(String nom, int numero, int jour, int mois, int annee){
		return (Journal) em.createQuery("select j from Journal j where j.nom = :name and j.numero = :number and j.jour = :day and j.mois = :month and j.annee = :year").setParameter("nom", nom).setParameter("number", numero).setParameter("day", jour).setParameter("month", mois).setParameter("year", annee).getSingleResult();
	}

	/**
	 * Renvoie la liste des journaux d'un client dont le theme est t
	 * @param t
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	public Collection<Journal> getJournauxClientsByTheme(Client c,Theme t){
		return (Collection<Journal>) em.createQuery("select j from Journal j join j.possesseurs p where p.username = :nom and j.theme.type = :theme").setParameter("nom", c.getNom()).setParameter("theme", t.getType()).getResultList();
	}

	/**
	 * Renvoie la liste des client dqui ont le journal J
	 * @param j
	 * @param login 
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	public Collection<Client> getClientByJournal(Journal j, String login){
		Collection<Client> clients = (Collection<Client>) em.createQuery("select c from Client c join c.dons d where d.id = :id").setParameter("id", j.getId()).getResultList();
		clients.remove((Client)this.getClient(login));
		return clients;
	}

	public Journal getJournalByName(String nom, int numero){
		return (Journal) em.createQuery("select j from Journal j where j.nom = :nom and j.numero = :num").setParameter("nom", nom).setParameter("num", numero).getSingleResult();
	}

	public int getTheme(String name){
		return (Integer) em.createNativeQuery("Select id from Theme where type = '"+name+"'").getSingleResult();
	}
	//à modifier ne marche pas
	@SuppressWarnings("unchecked")
	public Collection<Journal> getJournauxbyTheme(int ThemeId){
		return (Collection<Journal>) em.createQuery("select j from Journal j where j.theme.id = "+ThemeId).getResultList();
	}
	///////////////////////////////////////////////////////////////////////////////////////////////:
	// Gestion des journaux
	///////////////////////////////////////////////////////////////////////////////////////////////:


	public Journal nouveauJournal(String nom, String image_adresse, Theme theme, Integer numero, Integer jour, Integer mois, Integer annee ){
		Journal j = new Journal();


		j.setJour(jour);
		j.setMois(mois);
		j.setAnnee(annee);
		j.setImage_adresse(image_adresse);
		j.setNom(nom);
		j.setNumero(numero);
		j.setTheme(theme);

		em.persist(j);
		System.out.println("ajout du journal " + nom +" ok");

		return j;
	}

	public Journal modifierJournal(Integer id,String nom, String image_adresse, Theme theme, Integer numero,Integer jour, Integer mois, Integer annee ){
		Journal j = this.getJournal(id);

		j.setJour(jour);
		j.setMois(mois);
		j.setAnnee(annee);
		j.setImage_adresse(image_adresse);
		j.setNom(nom);
		j.setNumero(numero);
		j.setTheme(theme);

		em.merge(j);
		System.out.println("modification du journal " + nom +" ok");

		return j;
	}

	public void SupprimerJournal(Integer id){
		em.remove(this.getJournal(id));
	}

	public Journal getJournal(Integer id) {
		// TODO Auto-generated method stub
		return em.find(Journal.class, id);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////:
	// Gestion des themes
	///////////////////////////////////////////////////////////////////////////////////////////////:

	public Theme getTheme(int id){
		return em.find(Theme.class, id);

	}

	public Theme ajouterTheme(String nom){

		if(nom.equals("")){
			return null;
		}else{
			Theme t = new Theme();
			t.setType(nom);
			Collection<Theme> lt = this.getListeThemes();
			Object[] lta = lt.toArray();
			boolean themeold = false;
			int i = 0;

			while(!themeold && i<lta.length){
				themeold=(nom.equals(((Theme)lta[i]).getType()));
				i++;
			}

			if(themeold){
				return ((Theme)lta[i-1]);
			}else{
				em.persist(t);
				return t;
			}
		}

	}

	public void SupprimerTheme(int id){

		Theme t = em.find(Theme.class, id);

		Collection<Journal> cj = this.getListeJournaux();

		Object[] lj = cj.toArray();

		boolean themeUnused = true;

		int i = 0;
		while(i<lj.length && themeUnused){
			themeUnused=(((Journal)lj[i]).getTheme()!=t);
			i++;
		}
		if(themeUnused){
			em.remove(t);
		}

	}
	///////////////////////////////////////////////////////////////////////////////////////////////:
	// Methodes utiles 
	///////////////////////////////////////////////////////////////////////////////////////////////:

	public void echange(String receveur, String donneur, Integer journal){
		Client r = em.find(Client.class, receveur );
		Client d = em.find(Client.class, donneur );
		Journal j = em.find(Journal.class, journal );

		Set<Journal> donsR = r.getDons();
		Set<Journal> donsD = d.getDons();
		Set<Client> possesseursJ = j.getPossesseurs();

		donsR.add(j);
		donsD.remove(j);
		possesseursJ.remove(d);
		possesseursJ.add(r);

		r.setDons(donsR);
		d.setDons(donsD);
		j.setPossesseurs(possesseursJ);
		em.merge(r);
		em.merge(d);
		em.merge(j);

	}

	public String login(String login, String mdp) {
		System.out.println("verification du login de " + login );
		if(login=="admin"){
			return "admin";
		}else{
			Client c = em.find(Client.class, login);
			if(c==null){
				return "BadLogin";
			}else{
				if(mdp.equals(c.getPassword())){
					return "OK";	
				}else{
					return "MauvaisPassword";

				}

			}
		}
	}

	public static String joinAdresses(Collection<Client> cl){
		if (cl.size()==0){
			return "";
		}
		else{
			Collection<String> adresseList = new ArrayList<String>();
			for (Client c : cl){
				adresseList.add(c.getAdresse());
			}
			String res = "";
			String separator = "|";
			for (String item: adresseList) {
				res = res.concat(item);
				res = res.concat(separator);
			}
			return res.substring(0, res.length() - separator.length());
		}
	}


	public void initBD(){
		this.nouveauClient("cha", "cha", "Hubin", "Clement", "clement.hubin@gmail.com", "52"," rue", "achille viadieu", "31400", "toulouse");
		this.nouveauClient("matpiz", "matpiz", "Pizenberg", "Matthieu", "matthieu.pizengerg@gmail.com", "7","boulevard", "michelet", "31400", "toulouse");
		this.nouveauClient("mariilou", "mariilou", "Leconte", "Marie", "marie.leconte@gmail.com", "35","rue", "saint sylve", "31500", "toulouse");
		this.nouveauClient("patate", "patate", "LaFrite", "Jean-Marie", "jeanmarie.lafrite@gmail.com", "50","rue", "bayard", "31000", "toulouse");
		this.nouveauClient("carotte", "carotte", "Leconte", "Marie", "marie.leconte@gmail.com", "35","rue", "saint sylve", "31500", "toulouse");
		this.nouveauClient("aJmon", "dany", "Hagimont", "Daniel", "daniel.hagimont@gmail.com", "2","rue", "camichel", "31000", "toulouse");
		this.nouveauClient("flamby", "aheu", "Hollande", "François", "francois.hollande@gmail.com", "52","place", "capitole", "31000", "toulouse");
		this.nouveauClient("grenouille", "croa", "miss", "météo", "miss.meteo@gmail.com", "4","place", "saint pierre", "31000", "toulouse");
		this.nouveauClient("fauvette", "hihan", "Jean", "Rochefort", "jean.rochefort@gmail.com", "1","chemin", "des courses", "31100", "toulouse");
		this.nouveauClient("guillaume", "guillaume", "Guillaume", "Bouzyd", "guillaume.bouzyd@etu.enseeiht.fr", "34","rue", "de l'étoilde", "31000", "toulouse");

		Theme actu = this.ajouterTheme("Actualites");
		Theme auto = this.ajouterTheme("Automobile");
		Theme nature = this.ajouterTheme("Nature");
		Theme sciences = this.ajouterTheme("Sciences");
		Theme porn = this.ajouterTheme("Adulte");
		Theme turf = this.ajouterTheme("Turf");
		Theme culture = this.ajouterTheme("Culture");
		Theme photographie = this.ajouterTheme("Photographie");

		this.nouveauJournal("Liberation","http://www.cndp.fr/crdp-creteil/images/stories/resistance/2013-4_Liberation_ed-de-Paris_%20n3_23081944_p1_coll-MRN,.jpg",actu , 3, 23, 8, 1944);
		this.nouveauJournal("Le Figaro","http://autourduperetanguy.blogspirit.com/images/medium_FIGARO_1854_n_1.image.jpg",actu , 1, 2, 4, 1854);
		this.nouveauJournal("Le Monde","http://boutique.lemonde.fr/media/catalog/product/cache/4/small_image/500x500/9df78eab33525d08d6e5fb8d27136e95/1/9/1944_12_19_001c_1.jpg",actu , 1, 18, 12, 1944);
		this.nouveauJournal("L'Humanité","http://gallica.bnf.fr/ark:/12148/bpt6k250186x/f1.highres",actu , 1, 18, 4, 1904);
		this.nouveauJournal("Science & Vie","http://pmcdn.priceminister.com/photo/Collectif-La-Science-Et-La-Vie-N-N-1-La-Science-Et-La-Vie-Revue-250284832_ML.jpg",sciences , 1, 1, 1, 1913);
		this.nouveauJournal("Nature","http://upload.wikimedia.org/wikipedia/commons/d/d6/Nature_cover,_November_4,_1869.jpg",nature , 1, 1, 1, 1869);
		this.nouveauJournal("Télérama","http://www.infonaissance.com/Photos/MH%20telerama.jpg",culture , 1, 2, 10, 1960);
		this.nouveauJournal("L'automobile Magazine","http://i.ebayimg.com/00/s/MTYwMFgxMTYz/z/C5kAAMXQw8hRaUD7/$T2eC16hHJHkFFl6yPzoYBR,UD7C8iQ~~60_35.JPG",auto , 176, 1, 12, 1960);
		this.nouveauJournal("Compétence photo", "http://www.editions-eni.fr/livres-competence-photo-ndeg-25-optimisez-la-nettete-de-vos-images/.3592cbf73c4faf4ed27688dc0cbed2ec.jpg",photographie , 26, 1, 1, 2009);


	}
	///////////////////////////////////////////////////////////////////////////////////////////////:
	// Gestion des clients
	///////////////////////////////////////////////////////////////////////////////////////////////:


	public Client nouveauClient(String username, String password, String nom, String prenom, String email, String numero,String typeVoie, String nomVoie, String codePostal, String ville) {

		if(username.equals("")){
			return null;
		}else{
			Object[] lc = this.getListeClient().toArray();
			int i = 0;
			boolean exists = false;
			while(!exists && i<lc.length){
				exists=(((Client)lc[i]).getUsername().equals(username));
				i++;
			}

			if(exists){
				return null;
			}else{
				Client c = new Client();

				c.setUsername(username);
				c.setPassword(password);
				c.setNom(nom);
				c.setPrenom(prenom);
				c.setEmail(email);
				c.setNomVoie(nomVoie);
				c.setNumero(numero);
				c.setTypeVoie(typeVoie);
				c.setCodePostal(codePostal);
				c.setVille(ville);		System.out.println(c.getUsername()+ " va etre enregistrer");

				em.persist(c);
				System.out.println("ajout " + username +" ok");

				return c;
			}
		}
	}

	public Client getClient(String login){
		return em.find(Client.class, login);
	}


	public void modifierClient(String username, String password, String nom, String prenom, String email, String numero,String typeVoie, String nomVoie, String codePostal, String ville) {

		Client c = em.find(Client.class, username);
		c.setPassword(password);
		c.setNom(nom);
		c.setPrenom(prenom);
		c.setEmail(email);
		c.setNomVoie(nomVoie);
		c.setNumero(numero);
		c.setTypeVoie(typeVoie);
		c.setCodePostal(codePostal);
		c.setVille(ville);
		System.out.println(c.getUsername()+ " va etre modifie");

		em.merge(c);
		System.out.println("modification " + username +" ok");

	}

	public void SupprimerClient(String login) {
		em.remove(this.getClient(login));

	}

	public void AjouterDon(Journal j, String login){
		Client c = em.find(Client.class, login);
		Set<Journal> collec = c.getDons();
		Set<Client> lj = j.getPossesseurs();
		lj.add(c);
		j.setPossesseurs(lj);
		collec.add(j);
		c.setDons(collec);

		System.out.println("dons taille : "+c.getDons().size() );
		em.merge(j);
		em.merge(c);
		Client cp = em.find(Client.class, login);

		System.out.println("apres merge : "+cp.getDons().size());
		em.flush();

	}






	//	public void echangerJournal(String donneur, String receveur, Journal journal){
	//		Collection<Journal> journauxdonneur = (this.getClient(donneur).getAchats());
	//		journauxdonneur.get()
	//	}
}
