package feedmykiosk;

import java.io.IOException;
import java.util.Collection;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.tools.ws.processor.model.Request;

/**
 * Servlet implementation class ControleurJournaux
 */
public class ControleurJournaux extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB AdministrationKiosque administration;
	/**
	 * @see HttpServlet#HttpServlet()
	 */


	public ControleurJournaux() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");

		if (login != null){

			request.setAttribute("user", administration.getClient(login));
		}

		String action = request.getParameter("action");

		if (action == null){
			request.getRequestDispatcher("Intro.jsp").forward(request, response);
		} else {

			if (action.equals("AjouterJournal")){

				request.setAttribute("listeThemes", administration.getListeThemes());
				request.setAttribute("adminbar", "ajoutJ");
				request.getRequestDispatcher("NouveauJournal.jsp").forward(request, response);

			}else if (action.equals("NouveauTheme")){
				request.setAttribute("adminbar", "ajoutT");
				request.getRequestDispatcher("NouveauTheme.jsp").forward(request, response);

			}else if (action.equals("ListNews")){
				request.setAttribute("adminbar", "modifJ");
				request.setAttribute("listeJournaux", administration.getListeJournaux());
				request.getRequestDispatcher("ListNews.jsp").forward(request, response);

			}else if (action.equals("ListThemes")){
				request.setAttribute("adminbar", "modifT");
				request.setAttribute("listeThemes", administration.getListeThemes());
				request.getRequestDispatcher("ListThemes.jsp").forward(request, response);

			} else if (action.equals("MyKiosk")){
				if(login == null){
					response.sendRedirect("/Feedmykiosk");
				}else{
					Client c = administration.getClient((String)session.getAttribute("login"));
					System.out.println("Client size : "+ c.getDons().size());
					Collection<Journal> lj = administration.getClientDons(c);
					System.out.println("lj : "+lj.size());

					request.setAttribute("ListeJournaux", lj);
					request.setAttribute("navbar", "kiosk");
					request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);
				}
			}else{

				response.sendRedirect("Erreur.jsp");

			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");

		if (login != null){

			request.setAttribute("user", administration.getClient(login));
		}

		String action = request.getParameter("action");

		if (action == null){

			request.getRequestDispatcher("Intro.jsp").forward(request, response);

		} else {

			if (action.equals("AjouterJournal")){

				System.out.println(request.getParameter("Theme"));
				Theme theme = administration.getTheme(Integer.parseInt(request.getParameter("Theme")));

				administration.nouveauJournal(request.getParameter("NomJournal"),request.getParameter("ImageJournal"),theme,Integer.parseInt(request.getParameter("NumeroJournal")),Integer.parseInt(request.getParameter("Jour")),Integer.parseInt(request.getParameter("Mois")),Integer.parseInt(request.getParameter("Annee")));
				request.setAttribute("listeJournaux", administration.getListeJournaux());
				request.setAttribute("adminbar", "modifJ");
				request.getRequestDispatcher("ListNews.jsp").forward(request, response);

			}else if(action.equals("Retour")){

				request.setAttribute("adminbar", "accueil");
				if(request.getParameter("Retour").equals("Accueil")){
					request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);	
				}else{
					request.getRequestDispatcher("Admin.jsp").forward(request, response);	

				}

			}else if(action.equals("AjouterTheme")){
				request.setAttribute("adminbar", "modifT");
				administration.ajouterTheme(request.getParameter("NomTheme"));
				request.setAttribute("listeThemes", administration.getListeThemes());
				request.getRequestDispatcher("ListThemes.jsp").forward(request, response);	

			}else if (action.equals("Modifier")){

				//On recupere le journal et on redirige vers la page de modification
				if(request.getParameter("journal")!=null){
					request.setAttribute("adminbar", "modifJ");
					request.setAttribute("listeThemes", administration.getListeThemes());
					request.setAttribute("journal", administration.getJournal(Integer.parseInt(request.getParameter("journal"))));
					request.getRequestDispatcher("ModifierJournal.jsp").forward(request, response);
				}else{
					request.setAttribute("listeJournaux", administration.getListeJournaux());
					request.setAttribute("adminbar", "modifJ");
					request.getRequestDispatcher("ListNews.jsp").forward(request, response);

				}

			}else if (action.equals("ModifierJournal")){
				// On va effectuer la modification puis rediriger vers la page de suppression

				administration.modifierJournal(Integer.parseInt((request.getParameter("id"))),request.getParameter("NomJournal"),request.getParameter("ImageJournal"),administration.getTheme(Integer.parseInt(request.getParameter("Theme"))),Integer.parseInt(request.getParameter("NumeroJournal")),Integer.parseInt(request.getParameter("Jour")),Integer.parseInt(request.getParameter("Mois")),Integer.parseInt(request.getParameter("Annee")));
				request.setAttribute("listeJournaux", administration.getListeJournaux());
				request.setAttribute("adminbar", "modifJ");
				request.getRequestDispatcher("ListNews.jsp").forward(request, response);

			}else if (action.equals("Supprimer")){

				if(request.getParameter("journal")!=null){
					System.out.println("journal " + Integer.parseInt(request.getParameter("journal")));
					administration.SupprimerJournal(Integer.parseInt(request.getParameter("journal")));
				}
				request.setAttribute("listeJournaux", administration.getListeJournaux());
				request.setAttribute("adminbar", "modifJ");
				request.getRequestDispatcher("ListNews.jsp").forward(request, response);

			}else if (action.equals("SupprimerTheme")){

				if(request.getParameter("theme")!=null){
					int theme = Integer.parseInt(request.getParameter("theme"));
					System.out.println("Theme : "+theme);
					administration.SupprimerTheme(theme);
				}
				request.setAttribute("listeThemes", administration.getListeThemes());
				request.setAttribute("adminbar", "modifT");
				request.getRequestDispatcher("ListThemes.jsp").forward(request, response);

			} else if (action.equals("AjouterKiosk")){

				request.setAttribute("listeThemes", administration.getListeThemes());
				request.setAttribute("listeJournaux", administration.getListeJournaux());
				request.getRequestDispatcher("AjoutKiosk.jsp").forward(request, response);

			} else if (action.equals("Ajouter")){

				if(request.getParameter("journal")!=null){
					Journal j = administration.getJournal(Integer.parseInt(request.getParameter("journal")));
					administration.AjouterDon(j,login);
				}
				request.setAttribute("ListeJournaux", administration.getClientDons(administration.getClient(login)));
				request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

			} else if (action.equals("Rechercher")){


				Journal journal = administration.getJournal(Integer.parseInt(request.getParameter("journal")));
				request.setAttribute("Journal",journal);
				Collection<Client> clients = administration.getClientByJournal(journal,login);
				request.setAttribute("ListeClients",clients );
				request.getRequestDispatcher("FeedmeResult.jsp").forward(request, response);

			}else if (action.equals("Troquer")){


				if(request.getParameter("client")!=null){
					administration.echange(login, request.getParameter("client"), Integer.parseInt(request.getParameter("journal")));
				}
				
				Client c = administration.getClient((String)session.getAttribute("login"));
				Collection<Journal> lj = administration.getClientDons(c);

				request.setAttribute("ListeJournaux", lj);
				request.setAttribute("navbar", "kiosk");
				request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

			}
		}
	}
}