package feedmykiosk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * 
 */

/**
 * @author gbouzyd
 *
 */

@Entity
public class Theme{
	
	 
	@Id @GeneratedValue(strategy=GenerationType.TABLE) 
	private int id;
	private String type;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy="theme")
	private List<Journal> journaux;

	public Theme(){
		super();
	}
	
	public int getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String theme) {
		this.type = theme;
	}
	
	
}