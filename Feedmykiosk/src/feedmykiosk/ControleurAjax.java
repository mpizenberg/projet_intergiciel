package feedmykiosk;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ControleurAjax
 */
@WebServlet("/ControleurAjax")
public class ControleurAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@EJB AdministrationKiosque administration;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControleurAjax() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String selectedItem = request.getParameter("selectedValue");
		int id = administration.getTheme(selectedItem);
		Collection<Journal> jcollection = administration.getJournauxbyTheme(id);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		try {
			for(Journal j:jcollection){
				out.println("<option value=\""+j.getId()+"\">"+j.getNom()+"</option>");
			}
		} finally {            
			out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
