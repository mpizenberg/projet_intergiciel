package feedmykiosk;


import java.io.IOException;
import java.sql.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Controleur du site web
 */
public class Controleur extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@EJB AdministrationKiosque administration;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controleur() {
		super();

	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");

		if (login != null){
			request.setAttribute("user", administration.getClient(login));
		}

		String action = request.getParameter("action");

		if (action == null){
			
			request.setAttribute("touslesclients", administration.getListeClient());
			request.getRequestDispatcher("Intro.jsp").forward(request, response);
		} else {

			if (action.equals("Infos")){
				if(login == null){
					response.sendRedirect("/Feedmykiosk");
				}else{
					request.setAttribute("navbar", "infos");
					request.getRequestDispatcher("MesInfos.jsp").forward(request, response);
				}
			}else if (action.equals("feedme")){

				if(login == null){
					response.sendRedirect("/Feedmykiosk");
				}else{
					request.setAttribute("journaux",administration.getListeJournaux());
					request.setAttribute("listeThemes", administration.getListeThemes());
					request.setAttribute("listeJournaux", administration.getListeJournaux());
					request.setAttribute("navbar", "feedme");
					request.getRequestDispatcher("FeedmeSearch.jsp").forward(request, response);
				}

			} else if (action.equals("Contact")){

				request.setAttribute("navbar", "contact");
				request.getRequestDispatcher("Contact.jsp").forward(request, response);

			}else if (action.equals("Accueil")){
				if(login == null){
					request.getRequestDispatcher("Intro.jsp").forward(request, response);
				}else{
					request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);
				}

			}else if (action.equals("AccueilAdmin")){
				request.setAttribute("adminbar", "accueil");
				request.getRequestDispatcher("Admin.jsp").forward(request, response);

			}else if(action.equals("Initialisation")){

				if(administration.getListeClient().size()==0){
				administration.initBD();
				request.setAttribute("adminbar", "init");
				request.getRequestDispatcher("InitialisationBD.jsp").forward(request, response);
				}else{
				request.setAttribute("adminbar", "init");
				request.getRequestDispatcher("InitialisationDBDejaFaite.jsp").forward(request, response);
				}
			}else{

				System.out.println("DoGet : rien ne va plus");
				response.sendRedirect("Erreur.jsp");
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");

		if (login != null){
			request.setAttribute("user", administration.getClient(login));
		}

		String action = request.getParameter("action");

		if (action == null){
			request.getRequestDispatcher("Intro.jsp").forward(request, response);
		} else {

			//Enregistrement d'un nouveau client
			if(action.equals("Login")){
				String loginIntro = (String) request.getParameter("login");
				String passwordIntro = (String) request.getParameter("password");


				// Si le login est Admin, on affiche la page d'admiistration, sinon on verifie le login et on affiche soit la page d'acceuil, soit une page d'erreur.
				if (loginIntro.equals("Admin")){
					request.setAttribute("adminbar", "accueil");
					request.getRequestDispatcher("Admin.jsp").forward(request, response);

				}else if(administration.login(loginIntro,passwordIntro).equals("OK")){

					session.setAttribute("login", loginIntro);
					request.setAttribute("user", administration.getClient((String)session.getAttribute("login")));

					request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

				}else if(administration.login(loginIntro,passwordIntro).equals("MauvaisPassword")){

					response.sendRedirect("MauvaisPassword.jsp");

				}else{

					response.sendRedirect("MauvaisLogin.jsp");
				}
			}else if(action.equals("ModifierClient")){	

				administration.modifierClient(login,request.getParameter("password"),request.getParameter("nom"),request.getParameter("prenom"),request.getParameter("email"),request.getParameter("NumeroRue"),request.getParameter("TypeVoie"),request.getParameter("NomVoie"),request.getParameter("CodePostal"),request.getParameter("NomVille"));
				request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

			}else if(action.equals("Retour")){

				response.sendRedirect("/Feedmykiosk");

			}else if(action.equals("Logout")){

				if (login != null){
					request.setAttribute("user",null);
				}
				session.setAttribute("login", null );
				request.setAttribute("touslesclients", administration.getListeClient());
				request.getRequestDispatcher("Intro.jsp").forward(request, response);
			}else{
				response.sendRedirect("Erreur.jsp");
				doGet(request, response);
			}
		}

	}

}
