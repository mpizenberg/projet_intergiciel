package feedmykiosk;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 
 */

/**
 * @author gbouzyd
 *
 */
@Entity
public class Requete {
	@Id @GeneratedValue(strategy=GenerationType.AUTO) private int id;
	private Date date;
	@OneToOne private Journal journal;
	@OneToOne private Client donneur;
	@OneToOne private Client receveur;
	/**Constructeur
	 * 
	 */
	public Requete() {
		super();
	}
	
	//Getters and Setters
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the journal
	 */
	public Journal getJournal() {
		return journal;
	}
	/**
	 * @param journal the journal to set
	 */
	public void setJournal(Journal journal) {
		this.journal = journal;
	}
	/**
	 * @return the donneur
	 */
	public Client getDonneur() {
		return donneur;
	}
	/**
	 * @param donneur the donneur to set
	 */
	public void setDonneur(Client donneur) {
		this.donneur = donneur;
	}
	/**
	 * @return the receveur
	 */
	public Client getReceveur() {
		return receveur;
	}
	/**
	 * @param receveur the receveur to set
	 */
	public void setReceveur(Client receveur) {
		this.receveur = receveur;
	}
	
	
}
