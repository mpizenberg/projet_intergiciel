var geocoder;
var map;
var adresse = $('#adresses0').html();
//alert("première adresse = "+adresse);
var taille = $('#taille').html();
var coord;
function initialize() {
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': adresse}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			setMap(results);
		} else {
			var mapOptions = {
					zoom: 8,
					center: new google.maps.LatLng(43.6161263, 1.4566661),
					scaleControl: true
			}
			map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
			google.maps.event.addDomListener(window, 'load', mapOptions);
		}
	});
}

function setMap(result) {
	coord = result[0].geometry.location;
	var mapOptions = {
			zoom: 8,
			center: coord,
			scaleControl: true
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	var marqueur = new google.maps.Marker({
        position: coord,
        map: map,
        icon : 'IMG/logo_pin.png'
    });
	for(var i=1;i<taille;i++){
		adresse = $('#adresses'+i).html();
		//alert("adresse "+i+" = "+adresse);
		geocoder.geocode( { 'address': adresse}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				coord = results[0].geometry.location;
				var marqueur = new google.maps.Marker({
			        position: coord,
			        map: map,
			        icon : 'IMG/logo_pin.png'
			    });
			} else {
				
			}
		});
	}
	google.maps.event.addDomListener(window, 'load', mapOptions);
}
initialize();