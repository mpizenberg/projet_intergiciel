<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />

<h2 class="text-center titre-contact">Service Client</h2>
<div class="col-sm-offset-1 col-sm-8 col-xs-12">
	<div class="row">
		<form method="post" action="controleur"
			class="form-horizontal col-sm-12" role="form">
			<div class="form-group">
				<div class="col-sm-offset-2 col-md-offset-2 col-sm-12">
					<p>Pour toute question, n'hésitez pas à contacter notre service
						client en remplissant le formulaire ci-dessous.</p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Nom</label>
				<div class="col-sm-9 col-md-9">
					<p class="form-control-static"></p>
					<input type="text" name="nom" placeholder="mariilou"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Email</label>
				<div class="col-sm-9 col-md-9">
					<p class="form-control-static"></p>
					<input type="email" name="email" placeholder="email"
						class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-md-3 control-label">Message</label>
				<div class="col-sm-9 col-md-9">
					<p class="form-control-static"></p>
					<textarea name="message" rows="3" cols="50" placeholder="message"
						class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-md-offset-3 col-sm-6">
					<button type="submit" value="Modifier" class="btn btn-success">Envoyer</button>
				</div>
			</div>
		</form>
	</div>
</div>
<c:import url="Footer.jsp" />
<c:import url="HtmlFooter.jsp" />