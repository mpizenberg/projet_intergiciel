<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />

<p>Veuillez ajouter vos journaux/magazines</p>

<form method="post" action="controleurJournaux">
	<c:import url="SearchKiosk.jsp"></c:import>
	<p>
		<input type="submit" name="action" value="Ajouter">
	</p>
	<p>
		<input type="submit" name="action" value="Retour">
	</p>
	<p>
		<input type="hidden" name="Retour" value="Accueil">
	</p>
</form>


<c:import url="Footer.jsp" />
<script type="text/javascript" src="JS/ChangeSelector.js"></script>
<c:import url="HtmlFooter.jsp" />