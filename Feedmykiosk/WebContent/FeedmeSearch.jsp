<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />

<c:set var="GoogleKey" value="AIzaSyD-cES2ZOc2wZWqGiOubIhhKYO0BBE19YY" />
<div id="adresse" hidden>${user.adresse}</div>


<p>Voici les journaux disponibles les plus près de chez vous</p>
<form method="post" action="controleurJournaux">
	<c:import url="SearchKiosk.jsp"></c:import>
	<p>
		<input type="submit" name="action" value="Rechercher">
	</p>
</form>

<div id="map-canvas">
	<span id="loader" style="display: none;"><img
		src="IMG/ajax-loader.gif" alt="loading" /></span>
</div>



<c:import url="Footer.jsp" />
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=${GoogleKey}&sensor=true"></script>
<script type="text/javascript" src="JS/setUpMap.js"></script>
<script type="text/javascript" src="JS/ChangeSelector.js"></script>
<c:import url="HtmlFooter.jsp" />