<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		</div>
	</div>
</div>

<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-xs-12">
				<p>Powered by Java</p>
			</div>
			<div class="col-sm-4 col-xs-12">
				<p>
					Thank you for being here <span class="glyphicon glyphicon-heart"></span><br/>
					We love you
				</p>
			</div>
			<div class="col-sm-4 col-xs-12">
				<p>HandMade in Toulouse by :
					<ul class="list-unstyled list-inline">
						<li><abbr title="Guillaume Bouzyd">Guigui</abbr></li>
						<li><abbr title="Cl�ment Hubin Andrieux">Cha</abbr></li>
						<li><abbr title="Marie Leconte">Mariilou</abbr></li>
						<li><abbr title="Matthieu Pizenberg">Piz</abbr></li>
					</ul>
				</p>
			</div>
		</div>
	</div>
</div>


<link href="<c:url value="/CSS/style.css"/>" rel="stylesheet">
<link href="<c:url value="/CSS/bootstrap.min.css"/>" rel="stylesheet">

<script src="JS/jquery.min.js"></script>
<script src="JS/bootstrap.min.js"></script>