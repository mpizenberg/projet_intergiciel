<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="admin-bar" class="col-md-3 col-sm-4 col-xs-12">
	<ul class="nav nav-pills nav-stacked">
		<li class="<c:if test="${adminbar == 'accueil'}">active</c:if>">
			<a href="controleur?action=AccueilAdmin">Accueil Admin</a>
		</li>
		<li class="<c:if test="${adminbar == 'ajoutJ'}">active</c:if>">
			<a href="controleurJournaux?action=AjouterJournal">Ajouter un journal</a>
		</li>
		<li class="<c:if test="${adminbar == 'modifJ'}">active</c:if>">
			<a href="controleurJournaux?action=ListNews">Modifier/Supprimer un journal</a>
		</li>
		<li class="<c:if test="${adminbar == 'ajoutC'}">active</c:if>">
			<a href="controleurClients?action=AdminNouveauClient">Ajouter un client</a>
		</li>
		<li class="<c:if test="${adminbar == 'modifC'}">active</c:if>">
			<a href="controleurClients?action=ListClients">Modifier/Supprimer un client</a>
		</li>
		<li class="<c:if test="${adminbar == 'ajoutT'}">active</c:if>">
			<a href="controleurJournaux?action=NouveauTheme">Ajouter un theme</a>
		</li>
		<li class="<c:if test="${adminbar == 'modifT'}">active</c:if>">
			<a href="controleurJournaux?action=ListThemes">Supprimer un theme</a>
		</li>
		<li class="<c:if test="${adminbar == 'init'}">active</c:if>">
			<a href="controleur?action=Initialisation">Initialiser la BD</a>
		</li>
	</ul>
</div>