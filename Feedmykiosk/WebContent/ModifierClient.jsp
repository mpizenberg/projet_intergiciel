<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>	
<c:import url="AdminSideBarre.jsp"/>
	

<div class="col-md-9 col-sm-8 col-xs-12">
	<h2 class="text-center">Modifier du client ${user.username}</h2>
	<div class="row">
		<form method="post" action="controleurClients" class="form-horizontal col-sm-12" role="form">
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Nom d'utilisateur</label>
				<div class="col-sm-8 col-md-9">
					<p class="form-control-static">${user.username}</p>
					<input type="hidden" name="username" value="${user.username}">
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Password</label>
				<div class="col-sm-8 col-md-9">
					<input type="password" name="password" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Nom</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="nom" class="form-control" value="${user.nom}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Prenom</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="prenom" class="form-control" value="${user.prenom}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Email</label>
				<div class="col-sm-8 col-md-9">
					<input type="email" name="email" placeholder="email" class="form-control" value="${user.email}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">N°</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" min="1" name="NumeroRue" class="form-control" value="${user.numero}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Type de voie</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="TypeVoie" class="form-control" value="${user.typeVoie}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Nom de la voie</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="NomVoie" class="form-control" value="${user.nomVoie}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Code Postal</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" min="1" name="CodePostal" class="form-control" value="${user.codePostal}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Ville</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="NomVille" class="form-control" value="${user.ville}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-md-offset-3 col-sm-6">
					<button type="submit" value="Modifier" class="btn btn-success">Modifier</button>
					<input type="hidden" name="action" value="ModifierClientAdmin">
				</div>
			</div>
		</form>
	</div>
</div>

<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>