<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Test</title>
</head>

<body>
	<c:import url="Header.jsp" />
	
	<div class="container">

		<div class="text-center">
			<h1>Bootstrap starter template</h1>
			<p class="lead">
				Use this document as a way to quickly start any new project.<br>
				All you get is this text and a mostly barebones HTML document.
			</p>
			<p><c:out value="test" /></p>
		</div>

	</div>
	<!-- /.container -->
</body>

<link href="CSS/bootstrap.min.css" rel="stylesheet">
<link href="CSS/style.css" rel="stylesheet">
</html>