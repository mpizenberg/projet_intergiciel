<%@ tag body-content="tagdependent"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="type" required="true"%>
<%@ attribute name="name" required="true"%>
<%@ attribute name="label" required="true"%>

<div class="form-group">
	<label class="col-sm-4 col-md-3 control-label">${label}</label>
	<div class="col-sm-8 col-md-9">
		<input type="${type}" name="${name}" class="form-control">
	</div>
</div>