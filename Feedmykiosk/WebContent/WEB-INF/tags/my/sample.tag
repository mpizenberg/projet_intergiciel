<%@ tag body-content="tagdependent" %>

<%@ variable name-given="testUser" scope="NESTED" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Test</title>
</head>

<body>
	<c:import url="Header.jsp" />
	
	<div class="container">
	${testUser}
	<c:set var="testUser" value="2"/>
		<div class="text-center">
			<jsp:doBody/>
		</div>
	<c:set var="testUser" value="3"/>
	${testUser}

	</div>
	<!-- /.container -->
</body>

<link href="CSS/bootstrap.min.css" rel="stylesheet">
<link href="CSS/style.css" rel="stylesheet">
</html>