<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<form method="post" action = "controleurClients">

<p>Il semble que vous n'êtes pas inscrit, voulez vous faire plus ample connaissance ?</p>
<p><input type="submit"  value="Créer"></p>
<p><input type="hidden"  name=retour value=Acceuil></p>
<p><input type="hidden"  name=action value=MauvaisLogin></p>

<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>