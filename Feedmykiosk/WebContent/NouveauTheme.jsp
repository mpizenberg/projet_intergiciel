<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/my" prefix="my"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<c:import url="AdminSideBarre.jsp"/>

<div class="col-md-9 col-sm-8 col-xs-12">
	<h2 class="text-center">Ajouter un thème</h2>
	<div class="row">
		<form method="post" action="controleurJournaux" class="form-horizontal col-sm-12" role="form">
			<my:form-input type="text" name="NomTheme" label="Nom du thème"/>
			<my:form-button-create value="AjouterTheme"/>
		</form>
	</div>
</div>


<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>