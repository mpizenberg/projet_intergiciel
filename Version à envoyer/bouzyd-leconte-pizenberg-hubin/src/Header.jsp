<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="wrap">

	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<c:url value="/"/>">
					<img alt="Feedmykiosk" src="IMG/logo_feedmykiosk.png" height="64">
				</a>
				<div class="hidden-md hidden-lg" id="brandName"><h1>Feedmykiosk</h1></div>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<c:url value="controleurJournaux" var="kiosk">
						<c:param name="action" value="MyKiosk" />
					</c:url>
					<c:url value="controleur" var="feedme">
						<c:param name="action" value="feedme" />
					</c:url>
					<c:url value="controleur" var="info">
						<c:param name="action" value="Infos" />
					</c:url>
					<c:url value="controleur" var="contact">
						<c:param name="action" value="Contact" />
					</c:url>
					
					<li class="<c:if test="${navbar == 'kiosk'}">active</c:if>"><a href="${kiosk}">My kiosk</a></li>
					<li class="<c:if test="${navbar == 'feedme'}">active</c:if>"><a href="${feedme}">Feed me</a></li>
					<li class="<c:if test="${navbar == 'infos'}">active</c:if>"><a href="<c:out value="${info}" />">Mon profil</a></li>
					<li class="<c:if test="${navbar == 'contact'}">active</c:if>"><a href="<c:out value="${contact}" />">Contact</a></li>
					
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<c:if test="${user != null}">
						<li>
							<span id="user" class="glyphicon glyphicon-user">
								Salut ${user.username} !
							</span>
							<form id="form-logout" class="navbar-form form-inline" role="form"
								method="post" action="/Feedmykiosk/controleur">
								<button class="btn btn-danger" type="submit">
									<span class="glyphicon glyphicon-remove"></span>
								</button>
								<input type="hidden" name=action value=Logout>
							</form>
						</li>
					</c:if>
					<c:if test="${user == null}">
						<li><c:import url="FormLogin.jsp" /></li>
					</c:if>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div class="container">
		<div class="row">