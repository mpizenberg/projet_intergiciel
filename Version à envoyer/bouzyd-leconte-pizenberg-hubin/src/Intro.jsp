<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />

<c:set var="GoogleKey" value="AIzaSyD-cES2ZOc2wZWqGiOubIhhKYO0BBE19YY" />
<c:set var="compteur" value="0" />

<c:forEach items="${touslesclients}" var="c">
	<div id="adresses${compteur}" hidden>${c.adresse}</div>
	<c:set var="compteur" value="${compteur + 1}" />
</c:forEach>

<div id="map-canvas">
	<span id="loader" style="display: none;"><img
		src="IMG/ajax-loader.gif" alt="loading" /></span>
</div>

<div id="taille" hidden>${compteur}</div>

<c:import url="Footer.jsp" />
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=${GoogleKey}&sensor=false"></script>
<script type="text/javascript" src="JS/setUpMultipleMarkers.js"></script>
<c:import url="HtmlFooter.jsp" />