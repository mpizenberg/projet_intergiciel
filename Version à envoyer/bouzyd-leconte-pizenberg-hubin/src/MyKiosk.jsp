<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/my" prefix="my"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<form method="post" action="controleurJournaux">
<c:forEach items="${ListeJournaux}" var="j">
	<my:journal journal="${j}"/>
</c:forEach>

<p>Pour ajouter vos magazines </p>
<p><input type="submit"  value="Ajouter un journal au Kiosk"></p>
<p><input type="hidden" name="action"  value="AjouterKiosk"></p>
</form>

<c:import url="Footer.jsp"/>
<link href="<c:url value="/CSS/kiosk.css"/>" rel="stylesheet">
<c:import url="HtmlFooter.jsp"/>
 