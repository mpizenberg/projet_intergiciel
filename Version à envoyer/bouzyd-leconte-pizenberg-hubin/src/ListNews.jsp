<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<c:import url="AdminSideBarre.jsp"/>

<div class="col-md-9 col-sm-8 col-xs-12">
	<form method="post" action = "controleurJournaux" role="form">
		<fieldset>
		<legend>Liste des journaux</legend>
			<div class="form-group">
				<c:forEach items="${listeJournaux}" var="j">
					<div class="radio"><label>
						<input type="radio" name="journal" value="${j.id}" >
						${j.jour}-${j.mois}-${j.annee} ${j.nom} <img alt="Image du journal" src="${j.image_adresse}" width="300">
					</label></div>
				</c:forEach>
			</div>
			<div class="form-group">
				<div class="">
					<button type="submit"  name="action" class="btn btn-success" value="Modifier">Modifier</button>
					<button type="submit"  name="action" class="btn btn-danger" value="Supprimer">Supprimer</button>
					<button type="submit"  name="action" class="btn btn-default" value="Retour">Retour</button>
					<input type="hidden" name="Retour" value="Admin">
				</div>
			</div>
		</fieldset>
	</form>
</div>

<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>