<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/my" prefix="my"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />

<c:if test="${retour == 'Admin'}">
	<c:import url="AdminSideBarre.jsp" />
</c:if>

<div class="<c:if test="${retour == 'Admin'}">col-md-9 col-sm-8 col-xs-12</c:if><c:if test="${retour != 'Admin'}">col-sm-12</c:if>">
	<h2 class="text-center">Ajouter un client</h2>
	<div class="row">
		<form method="post" action="controleurClients" class="form-horizontal col-sm-12" role="form">
			<my:form-input type="text" name="username" label="Nom d'utilisateur"/>
			<my:form-input type="password" name="password" label="Password"/>
			<my:form-input type="text" name="nom" label="Nom"/>
			<my:form-input type="text" name="prenom" label="Prenom"/>
			<my:form-input type="email" name="email" label="Email"/>
			<my:form-input type="number" name="NumeroRue" label="N°"/>
			<my:form-input type="text" name="TypeVoie" label="Type de voie"/>
			<my:form-input type="text" name="NomVoie" label="Nom de la voie"/>
			<my:form-input type="number" name="CodePostal" label="Code Postal"/>
			<my:form-input type="text" name="NomVille" label="Ville"/>
			<div class="form-group">
				<div class="col-sm-offset-4 col-md-offset-3 col-sm-6">
					<button type="submit" value="Creer" class="btn btn-success">Créer</button>
					<input type="hidden" name="retour" value="${retour}">
					<input type="hidden" name="action" value="NouveauClient">
				</div>
			</div>
		</form>
	</div>
</div>

<c:import url="Footer.jsp" />
<c:import url="HtmlFooter.jsp" />