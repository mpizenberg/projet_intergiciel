<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/my" prefix="my"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<c:import url="AdminSideBarre.jsp"/>

<div class="col-md-9 col-sm-8 col-xs-12">
	<h2 class="text-center">Ajouter un journal</h2>
	<div class="row">
		<form method="post" action="controleurJournaux" class="form-horizontal col-sm-12" role="form">
			<my:form-select name="NomJournal" list="${listeThemes}"/>
			<my:form-input type="text" name="NomJournal" label="Nom du journal"/>
			<my:form-input type="number" name="NumeroJournal" label="Numéro du Journal"/>
			<my:form-input type="number" name="Jour" label="Jour"/>
			<my:form-input type="number" name="Mois" label="Mois"/>
			<my:form-input type="number" name="Annee" label="Année"/>
			<my:form-input type="text" name="ImageJournal" label="Image du Journal"/>
			<my:form-button-create value="AjouterJournal"/>
		</form>
	</div>
</div>


<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>