<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>

<c:import url="AdminSideBarre.jsp"/>

<div class="col-md-9 col-sm-8 col-xs-12">
	<form method="post" action = "controleurJournaux" role="form">
		<fieldset>
		<legend>Liste des thèmes</legend>
			<div class="form-group">
				<c:forEach items="${listeThemes}" var="t">
					<div class="radio"><label>
						<input type="radio" name="theme" value="${t.id}">
						${t.type}
					</label></div>
				</c:forEach>
			</div>
			<div class="form-group">
				<div class="">
					<button type="submit"  name="action" class="btn btn-danger" value="SupprimerTheme">Supprimer</button>
					<button type="submit"  name="action" class="btn btn-default" value="Retour">Retour</button>
					<input type="hidden" name="Retour" value="Admin">
				</div>
			</div>
		</fieldset>
	</form>
</div>

<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>