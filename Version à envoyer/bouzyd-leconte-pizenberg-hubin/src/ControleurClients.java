package feedmykiosk;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ControleurClients
 */
@WebServlet(name = "controleurClients", urlPatterns = { "/controleurClients" })
public class ControleurClients extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB AdministrationKiosque administration;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControleurClients() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String login = request.getParameter("login");
		if (login != null){
			request.setAttribute("user", administration.getClient(login));
		}
		String action = request.getParameter("action");
		if (action == null){
			request.getRequestDispatcher("Intro.jsp").forward(request, response);
		} else {

			if(action.equals("NouveauClient")){
				response.sendRedirect("NouveauClient.jsp");

			}else if (action.equals("MyKiosk")){

				//	request.setAttribute("ListeJournaux", administration.getClientJournaux(administration.getClient(request.getParameter("login"))));
				request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

			}else if (action.equals("AjoutKiosk")){

				request.setAttribute("ListeJournaux", administration.getListeJournaux());
				request.setAttribute("ListeThemes", administration.getListeThemes());

				request.getRequestDispatcher("AjoutKiosk.jsp").forward(request, response);

			}else if (action.equals("ListClients")){

				request.setAttribute("listeClients", administration.getListeClient());
				request.setAttribute("adminbar", "modifC");
				request.getRequestDispatcher("ListClients.jsp").forward(request, response);

			}else if(action.equals("AdminNouveauClient")){
				request.setAttribute("retour", "Admin");
				request.setAttribute("adminbar", "ajoutC");
				request.getRequestDispatcher("NouveauClient.jsp").forward(request, response);	

			}else{
				System.out.println("DoGet : rien ne va plus");

				response.sendRedirect("Erreur.jsp");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		String login = (String) session.getAttribute("login");
		
		if (login != null){
			request.setAttribute("user", administration.getClient(login));
		}
		
		String action = request.getParameter("action");
		
		if (action == null){
			request.getRequestDispatcher("Intro.jsp").forward(request, response);
		} else {

			//Enregistrement d'un nouveau client
			if(action.equals("NouveauClient")){

				Client c = administration.nouveauClient(request.getParameter("username"),request.getParameter("password"),request.getParameter("nom"),request.getParameter("prenom"),request.getParameter("email"),request.getParameter("NumeroRue"),request.getParameter("TypeVoie"),request.getParameter("NomVoie"),request.getParameter("CodePostal"),request.getParameter("NomVille"));
				System.out.println("retourcontroleur");

				String retour = request.getParameter("retour");
				System.out.println(retour);

				if (retour.equals("Admin")){
					request.setAttribute("listeClients", administration.getListeClient());
					request.setAttribute("adminbar", "modifC");
					request.getRequestDispatcher("ListClients.jsp").forward(request, response);
				
				}else if (retour.equals("Accueil")){

					session.setAttribute("login",c.getUsername());
					request.setAttribute("user", c);
					request.getRequestDispatcher("MyKiosk.jsp").forward(request, response);

				}else{
					response.sendRedirect("Erreur.jsp");
				}


			}else if(action.equals("Retour")){
				request.setAttribute("adminbar", "accueil");
				request.getRequestDispatcher("Admin.jsp").forward(request, response);

			}else if(action.equals("MauvaisLogin")){
				
				request.setAttribute("retour", "Accueil");
				request.getRequestDispatcher("NouveauClient.jsp").forward(request, response);	


			}else if (action.equals("Supprimer")){
				if(request.getParameter("client")!=null){

				administration.SupprimerClient(request.getParameter("client"));
				}
				request.setAttribute("listeClients", administration.getListeClient());
				request.setAttribute("adminbar", "modifC");
				request.getRequestDispatcher("ListClients.jsp").forward(request, response);
				// On va modifier un  client

			}else if(action.equals("Modifier")){
				if(request.getParameter("client")!=null){

				request.setAttribute("user", administration.getClient(request.getParameter("client")));
				request.setAttribute("adminbar", "modifC");
				request.getRequestDispatcher("ModifierClient.jsp").forward(request, response);
				}else{
					request.setAttribute("listeClients", administration.getListeClient());
					request.setAttribute("adminbar", "modifC");
					request.getRequestDispatcher("ListClients.jsp").forward(request, response);
				}
			}else if(action.equals("ModifierClientAdmin")){
				
				administration.modifierClient(request.getParameter("username"),request.getParameter("password"),request.getParameter("nom"),request.getParameter("prenom"),request.getParameter("email"),request.getParameter("NumeroRue"),request.getParameter("TypeVoie"),request.getParameter("NomVoie"),request.getParameter("CodePostal"),request.getParameter("NomVille"));
				request.setAttribute("adminbar", "modifC");
				request.setAttribute("listeClients", administration.getListeClient());
				request.getRequestDispatcher("ListClients.jsp").forward(request, response);
				
			}else{
				
				response.sendRedirect("Erreur.jsp");
			}
		}
	}

}
