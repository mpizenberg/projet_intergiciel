<%@ tag body-content="tagdependent"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="value" required="true"%>

<div class="form-group">
	<div class="col-sm-offset-4 col-md-offset-3 col-sm-6">
		<button type="submit" class="btn btn-success">Cr�er</button>
		<input type="hidden" name="action" value="${value}">
	</div>
</div>