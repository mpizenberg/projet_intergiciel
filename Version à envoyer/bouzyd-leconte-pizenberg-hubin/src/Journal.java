package feedmykiosk;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;




/**
 * @author gbouzyd
 *
 */
@Entity
public class Journal{
	@Id @GeneratedValue(strategy=GenerationType.TABLE) 
	private int id;

	private String nom;

	@ManyToMany(fetch=FetchType.EAGER)
	private Set<Client> possesseurs;

	private String image_adresse;

	@ManyToOne(fetch=FetchType.EAGER)
	private Theme theme;

	private int numero;
	private int jour;
	private int mois;
	private int annee;

	/**Constructeur
	 * 
	 */
	public Journal() {
		super();
	}


	//getters and setters

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the image_adresse
	 */
	public String getImage_adresse() {
		return image_adresse;
	}
	/**
	 * @param image_adresse the image_adresse to set
	 */
	public void setImage_adresse(String image_adresse) {
		this.image_adresse = image_adresse;
	}


	/**
	 * @return the possesseur
	 */

	public	Set<Client> getPossesseurs(){
		return possesseurs;
	}



	public int getJour() {
		return jour;
	}


	public void setJour(int jour) {
		this.jour = jour;
	}


	public int getMois() {
		return mois;
	}


	public void setMois(int mois) {
		this.mois = mois;
	}


	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}


	/**
	 * @param possesseur the possesseur to set
	 */
	public void addPossesseur(Client possesseur) {
		this.possesseurs.add(possesseur);
	}

	public void removePossesseur(Client possesseur) {
		this.possesseurs.remove(possesseur);
	}


	public Theme getTheme() {
		return theme;
	}


	public void setTheme(Theme theme) {
		this.theme = theme;
	}


	public int getNumero() {
		return numero;
	}


	public void setNumero(int numero) {
		this.numero = numero;
	}


	public void setPossesseurs(Set<Client> lj) {
		this.possesseurs=lj;                                                                                                                                                                                                          		
	}


}
