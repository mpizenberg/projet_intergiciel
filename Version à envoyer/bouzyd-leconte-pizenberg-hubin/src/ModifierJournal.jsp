<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/my" prefix="my"%>

<c:import url="HtmlHeader.jsp"/>
<c:import url="Header.jsp"/>
<c:import url="AdminSideBarre.jsp"/>

<div class="col-md-9 col-sm-8 col-xs-12">
	<h2 class="text-center">Modification du Journal ${journal.nom}</h2>
	<div class="row">
		<form method="post" action="controleurJournaux" class="form-horizontal col-sm-12" role="form"> 
			<my:form-select name="Theme" list="${listeThemes}"/>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Nom du journal</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="NomJournal" class="form-control" value="${journal.nom}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Numéro du Journal</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" name="NumeroJournal" class="form-control" value="${journal.numero}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Jour</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" name="Jour" class="form-control" value="${journal.jour}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Mois</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" name="Mois" class="form-control" value="${journal.mois}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Année</label>
				<div class="col-sm-8 col-md-9">
					<input type="number" name="Annee" class="form-control" value="${journal.annee}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-md-3 control-label">Image du Journal</label>
				<div class="col-sm-8 col-md-9">
					<input type="text" name="ImageJournal" class="form-control" value="${journal.image_adresse}"> <img alt="" src="${journal.image_adresse}">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-md-offset-3 col-sm-6">
					<input type="hidden" name="id" value="${journal.id}">
					<button type="submit" value="Modifier" class="btn btn-success">Modifier</button>
					<input type="hidden"  name="action" value="ModifierJournal">
				</div>
			</div>
		</form>
	</div>
</div>
<c:import url="Footer.jsp"/>
<c:import url="HtmlFooter.jsp"/>
