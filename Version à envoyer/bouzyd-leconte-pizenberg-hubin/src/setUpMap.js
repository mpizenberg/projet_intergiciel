var geocoder;
var map;
var adresse = $('#adresse').html();
var coord;
function initialize() {
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': adresse}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			setMap(results);
		}
	});
}
function setMap(result) {
	coord = result[0].geometry.location;
	var mapOptions = {
			zoom: 8,
			center: coord,
			scaleControl: true
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	var marqueur = new google.maps.Marker({
        position: coord,
        map: map,
        icon : 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });
	var infowindow = new google.maps.InfoWindow({
	      content: 'Vous êtes ici !'
	  });
	google.maps.event.addListener(marqueur, 'click', function() {
	    infowindow.open(map,marqueur);
	  });
	google.maps.event.addDomListener(window, 'load', mapOptions, carte);
}
initialize();