<%@ tag body-content="tagdependent"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="name" required="true"%>
<%@ attribute name="list" required="true" type="java.util.List"%>

<div class="form-group">
	<label class="col-sm-4 col-md-3 control-label">Th�me</label>
	<div class="col-sm-8 col-md-9">
		<select name="Theme" class="form-control">
			<c:forEach items="${list}" var="item">
				<option value="${item.id}">${item.type}</option>
			</c:forEach>
		</select>
	</div>
</div>