<%@ tag body-content="tagdependent" %>
<%@ attribute name="journal" required="true" type="feedmykiosk.Journal" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="journal">
	<div class="journal-head">
		<span class="journal-num badge">${journal.numero}</span>
	</div>
	<div class="journal-background">
		<img alt="${journal.nom}" src="${journal.image_adresse}">
	</div>
</div>