<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.*, feedmykiosk.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="HtmlHeader.jsp" />
<c:import url="Header.jsp" />
<c:set var="GoogleKey" value="AIzaSyD-cES2ZOc2wZWqGiOubIhhKYO0BBE19YY" />
<c:set var="compteur" value="0" />

<div id="adresse" hidden>${user.adresse}</div>

<p>Voici les personnes qui ont ce journal</p>
<form method="post" action="controleurJournaux">
	<div class="form-group">
		<c:forEach items="${ListeClients}" var="c">
				<div id="adresses${compteur}" hidden>${c.adresse}</div>
				<div id="usernames${compteur}" hidden>${c.username}</div>
				<c:set var="compteur" value="${compteur + 1}" />
				<div class="radio">
				<label> <input type="radio" name="client"
					value="${c.username}"> ${c.username},
					${c.email}
				</label>
			</div>
		</c:forEach>
	</div>
	<div class="form-group">
		<p>
			<input type="submit" name="action" value="Troquer" />
		</p>
		<p>
			<input type="hidden" name="journal" value="${Journal.id}">
		</p>
</form>

<div id="map-canvas">
	<span id="loader" style="display: none;"><img
		src="IMG/ajax-loader.gif" alt="loading" /></span>
</div>
<div id="taille" hidden>${compteur}</div>


<c:import url="Footer.jsp" />
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?key=${GoogleKey}&sensor=true"></script>
<script type="text/javascript" src="JS/MultipleMarkersFeedMe.js"></script>
<c:import url="HtmlFooter.jsp" />